void main() {
  //Set<dataType> variableName = {values};

  //A set in Dart is an unordered collection of unique items;
  Set<String> subcontractors = {'Sonderhoff', 'Stahlschmidt'};
  //to add something in objects
  subcontractors.add('Schweisstechnik');
  subcontractors.add('Kreatel');
  subcontractors.add('Kunstoffe');
  //to remove it in objects
  subcontractors.remove('Sonderhoff');
  print(subcontractors);
}
