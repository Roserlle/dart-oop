void main() {
  //List implements the concept of an orray
  //List allows duplicate values while sets do not
  //List elements can be accessed using a numeric index(ordered)
  //while set elements cannot be accessed using a numeric index(unordered)
  //Map implements the concept of an object
  List<int> discountRanges = [20, 40, 60, 80];
  List<String> names = ['John', 'Jane', 'Tom'];

  //list that cannot be changed
  const List<String> maritalStatus = [
    'single',
    'married',
    'divorced',
    'widowed'
  ];

  print(discountRanges);
  print(names);

  //to access element via index
  print(discountRanges[2]);
  print(names[2]);

  //highlight+ctrl+shift+d = copy
  //to determine the length
  print(discountRanges.length);
  print(names.length);

  //To change the value
  names[0] = 'Jonathan';
  print(names);

  //is like push() in JS
  names.add('Mark'); // add element to the end of the list
  names.insert(0, 'Roselle'); //add element to the beginning
  print(names.isEmpty);
  //output: false - if you want to check if there is an element in the list
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);
  //This is a method
  //This method is modifies the list itself
  names.sort();
  // names.sort((a, b) => a.length.compareTo(b.length));
  print(names.reversed); //reversed after the sort()

  //casting
  //string interpolation
  //convert to string to integer
  //int.parse(<identifier>)
}
