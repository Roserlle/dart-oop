void main() {
  //We use Map to represent an object, either physical or imaginary objects
  Map<String, String> address = {
    'specifics': '221B Baker Street',
    'district': 'MaryLebone',
    'city': 'London',
    'country': 'United Kingdom'
  };

  address['region'] = 'Europe';

  print(address);
}
