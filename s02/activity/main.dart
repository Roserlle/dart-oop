void main() {
  Map<String, dynamic> names = {
    'First Name': 'John',
    'Last Name': 'Smith',
    'Age': 34
  };

  Set<String> countries = {
    'Thailand',
    'Philippines',
    'Canada',
    'Singapore',
    'United Kingdom',
    'Japan'
  };

  List<String> laptops = [
    'Ryzen 5 3600',
    'Ryzen 3 3200G',
    'Ryzen 7 5800X',
    'Ryzen 9 5950X'
  ];

  print(names.length);
  print(countries.length);
  print(laptops.length);
}
