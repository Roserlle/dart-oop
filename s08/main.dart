void main() {
  Bulldozer bulldozer = new Bulldozer(
    equipmentType: 'bulldozer',
    model: 'Caterpillar D10',
    bladeType: 'U blade'
  );
  TowerCrane towerCrane = new TowerCrane(
    equipmentType: 'tower crane',
    model: '370 EC-B 12 Fibre',
    hookRadius: 78,
    capacity: 12000
  );
  Loader loader = new Loader(
    equipmentType: 'Loader',
    model: 'Volvo L60H',
    equipmentModel: 'wheel Loader',
    tippingLoad: 16530
  );

  List<Equipment> vehicles = [];
  vehicles.add(bulldozer);
  vehicles.add(towerCrane);
  vehicles.add(loader);

  //to test the equipment
  //print(equipment.isNotEmpty);
  vehicles.forEach((Equipment vehicle) {
    print(vehicle.describe());
  });
}

abstract class Equipment {
  String equipmentType;

  Equipment({required this.equipmentType});
  String describe();
}

class Bulldozer implements Equipment {
  String equipmentType;
  String model;
  String bladeType;

  Bulldozer({
    required this.equipmentType,
    required this.model,
    required this.bladeType
  });

  String describe() {
    return 'The ${this.equipmentType} ${this.model} has a ${this.bladeType}.';
  }
}

class TowerCrane implements Equipment {
  String equipmentType;
  String model;
  int hookRadius;
  int capacity;

  TowerCrane({
    required this.equipmentType,
    required this.model,
    required this.hookRadius,
    required this.capacity
  });

  String describe() {
    return 'The ${this.equipmentType} ${this.model} has a radius of ${this.hookRadius} and a max capacity of ${this.capacity}.';
  }
}

class Loader implements Equipment {
  String equipmentType;
  String model;
  String equipmentModel;
  int tippingLoad;

  Loader({
    required this.equipmentType,
    required this.model,
    required this.equipmentModel,
    required this.tippingLoad
  });

  String describe() {
    return 'The ${this.equipmentType} ${this.model} is a ${this.equipmentModel} and has a tipping load of ${this.tippingLoad} lbs.';
  }
}
