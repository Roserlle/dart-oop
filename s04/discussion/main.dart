//The main function is the entry point of dart application
void main() {
  // String companyName = getCompanyName();
  // print(companyName);
  // print(getCompanyName());
  // print(getYearEstablished());
  // print(hasOnlineClasses());
  // print(getCoordinates());
  // print(combineAddress(
  //     '134 Timog Avenue', 'Brgy. Sacred Heart', 'Quezon City', 'Metro Manila'));
  // print(combineName('John', 'Smith'));
  // print(combineName('John', 'Smith', isLastNameFirst: true));
  // print(combineName('John', 'Smith', isLastNameFirst: false));

  List<String> persons = ['John Doe', 'Jane Doe'];
  List<String> students = ['Nicholas Rush', 'James Holden'];

  //Anonymous function
  // persons.forEach((String person) {
  //   print(person);
  // });
  // students.forEach((String person) {
  //   print(person);
  // });

  //Functions can be treated as an object, that object can be used as an argument
  //Functions as objects AND used as an argument
  //printName(value) is function execution/call/invocation.
  //printName is reference to the given function
  persons.forEach(printName);
  students.forEach(printName);
  print(isUnderAge(18));
}

void printName(String name) {
  print(name);
}

//Optional named parameters
//These are parameters added after the required ones.
//These parameters are added inside a curly bracket
//If no value is given, a default value can be assigned
String combineName(String firstName, String lastName,
    {bool isLastNameFirst = false}) {
  if (isLastNameFirst) {
    return '$lastName, $firstName';
  } else {
    return '$firstName, $lastName';
  }
}

//A function are small units of code blocks that achieve a specific task.
//Then, the created functions can be used in other parts of a given program.
String combineAddress(
    String specifics, String barangay, String city, String province) {
  return '$specifics, $barangay, $city, $province';
}

String getCompanyName() {
  return 'FFUF';
}

int getYearEstablished() {
  return 2017;
}

bool hasOnlineClasses() {
  return true;
}

// bool isUnderAge(int age) {
//   return (age < 18) ? true : false;
// }

//The initial isUnderAge function can be changed into a lambda (arrow) function.
//A lambda function is a shortcut function for returning values from simple operations
//The syntax of a lambda function is:
//return-type function-name(parameters) => expression
bool isUnderAge(age) => (age < 18) ? true : false;

Map<String, double> getCoordinates() {
  return {'latitude': 14.632702, 'longitude': 121.043716};
}

//The following syntax is followed when creating a function
/* 
  return-type function-name(param-data-type parameterA, param-data-type parameterB){
    //code logic inside a function
    return 'return-value
  }
*/

//Arguments are the values being passed to the function.
//Parameters identify values that are passed into a function

// x (parameter) = 5 (argument)

//In Dart, writing the data type of a function parameter is not required.
//The reason is related to code readability and conciseness.
//If no data type is specified to a parameter, its default type is 'dynamic'
