void main() {
  Function discountBy25 = getDiscount(25);
  Function discountBy50 = getDiscount(50);
  //The discountBy25 is then considered as a closure.
  //The closure has access to a variables in its lexical scope.
  print(discountBy25(1400));
  print(discountBy50(1400));

  print(getDiscount(25)(1400));
  print(getDiscount(50)(1400));
}

Function getDiscount(num percentage) {
  //When the getDiscount is used and the function below is returned,
  //The value of 'percentage' parameter is retained.
  //That is lexical scope
  return (num amount) {
    return amount * percentage / 100;
  };
}
