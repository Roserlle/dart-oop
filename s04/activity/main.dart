void main() {
  List<num> prices = [45, 34.2, 176.9, 32.2];

  print(getSum(prices));
  print(getDiscount(20)(getSum(prices)));
  print(getDiscount(40)(getSum(prices)));
  print(getDiscount(60)(getSum(prices)));
  print(getDiscount(80)(getSum(prices)));
}

Function getDiscount(num percentage) {
  return (num amount) {
    return amount * percentage / 100;
  };
}

num getSum(prices) {
  num count = 0;
  for (num price in prices) {
    count += price;
  }
  return count;
}
