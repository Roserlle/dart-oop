void main() {
  //Assignment operator - add a value to a variable
  int x = 1397;
  int y = 7831;

  //Arithmetic operators
  num sum = x + y;
  num difference = x - y;
  num product = x * y;
  num quotient = x / y;
  num remainder = 4 % 3;
  num output = (x * y) - (x / y + x);
  print(sum);
  print(difference);
  print(product);
  print(quotient);
  print(remainder);
  print(output);

  //Relational Operators
  bool isGreaterThan = x > y;
  bool isLessThan = x < y;

  bool isGTorEqual = x >= y;
  bool isLTorEqual = x != y;
  bool isEqual = x == y;
  bool isNotEqual = x != y;

  //Logical Operators
  bool isLegalAge = true;
  bool isRegistered = false;

  bool areAllRequirementsMet = isLegalAge && isRegistered;
  bool areSomeRequirementsMet = isLegalAge || isRegistered;
  bool isNotRegistered = !isRegistered; //!(not) reverse the value

  print(x++);
  print(y--);
}
