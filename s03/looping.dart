void main() {
  // whileLoop();
  // doWhileLoop();
  // forLoop();
  // forInLoop();
  modifiedForLoop();
}

void whileLoop() {
  int count = 5;

  while (count != 0) {
    print(count);
    count--;
  }
}

void doWhileLoop() {
  int count = 20;
  do {
    print(count);
    count--;
  } while (count > 0);
}

void forLoop() {
  //for initializer, condition and iterator
  for (int count = 0; count <= 20; count++) {
    print(count);
  }
}

void forInLoop() {
  //List of Maps (in JS an array of Objects)
  List<Map<String, dynamic>> persons = [
    {'name': 'Brandon'},
    {'name': 'John'},
    {'name': 'Arthur'},
  ];

  for (var person in persons) {
    print(person['name']);
  }
}

//continuing term - branching
void modifiedForLoop() {
  for (int count = 0; count <= 20; count++) {
    if (count % 2 == 0) {
      continue;
    } else {
      print(count);
    }

    if (count > 10) {
      break;
    }
  }
}
