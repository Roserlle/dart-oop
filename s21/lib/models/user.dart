class User{
    late final int? id;
    late final String? email;
    late final String? accessToken;

    User({
        this.id,
        this.email,
        this.accessToken  
    });
}

//Dart does not check the values of the object,
//but also its unque identifier (through hash codes)