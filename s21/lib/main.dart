import './models/user.dart';
    void main() {

        User userA = User(id: 1);
        User userB = User(id: 1, email:'john@gmail.com');

        // print(userA.hashCode);
        // print(userB.hashCode);
        // print(userA == userB);

        //To check for object equality, we need to do it like the code below
        // bool isIdSame = userA.id == userB.id;
        // bool isEmailSame = userA.email == userB.email;
        // bool areObjectsSame = isIdSame == isEmailSame;
        
        // print(areObjectsSame);
        // print(userA.email);
        // userA.email = 'john@hotmail.com';
        // print(userA.email);
    }
    //Dart does not check values of the objedct,
    //but also its unique idetifier (through hash codes)