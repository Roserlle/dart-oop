import './worker.dart';

void main() {
  // Person personA = new Person();
  Doctor doctor = new Doctor(lastName: 'Smith', firstName: 'John');
  Carpenter carpenter = new Carpenter();

  print(carpenter.getType());
}

//concrete class - class na walang abstract class
//Abstract class defines what the concrete class should be implement, but does not tell you how to implement it
abstract class Person {
  //When we have abstract class, we can create abstract method
  //There is no implementation involved.
  //The class Person defines that a 'getFullName' must be implemented
  //However, it does not tell the concrete class HOW to implement it
  String getFullName();
}

//implementation of the abstract class, make it class concrete
//Keyword is implements
//The concrete class is the Doctor class
class Doctor implements Person {
  String firstName;
  String lastName;

  //If you remove the required, instance variable will remove the label
  Doctor({required this.firstName, required this.lastName});

  String getFullName() {
    return 'Dr. ${this.firstName} ${this.lastName}';
  }
}

class Attorney implements Person {
  String getFullName() {
    return 'Atty.';
  }
}
