import 'package:flutter/material.dart';
import './body_questions.dart';
import 'body_answer.dart';
void main() {
  // The App() is the root widget.
  runApp(App());
}
class App extends StatefulWidget {
  @override
  AppState createState() => AppState();
}
class AppState extends State<App> {
    int questionIdx = 0;
    bool showAnswers = false;


    final questions = [
    {
        'question':'What is the nature of you business needs?',
        'options': ['Time Tracking', 'Asset Management', 'Issue Tracking']
    },
    {
        'question': 'What is the expected size of the user base?',
        'options': ['Less than 1,000', 'Less than 10,000', 'More than 10,000']
    },
    {
        'question': 'In which region would the majority of the user base be?',
        'options': ['Asia', 'Europe', 'Americas', 'Africa', 'Middle East']
    },
    {
        'question': 'What is the expected project duration?',
        'options': ['Less than 3 months', '3-6 months', '6-9 months', 'More than 12 months']
    }
    ];

    var answers = [];

    //pede walang laman yung ireturn
    void nextQuestion(String? answer) {
        
        answers.add({
            'question': questions[questionIdx]['question'],
            'answer':(answer==null)? '': answer
        });
        print(answer);

        if(questionIdx < questions.length -1){
            setState(() => questionIdx++);
        } else {
            setState(() => showAnswers = true);
        }
        
    }
    @override
    Widget build(BuildContext context) {

    var bodyQuestions = BodyQuestions(questions: questions, questionIdx: questionIdx, nextQuestion: nextQuestion);
    var bodyAnswers = BodyAnswers(answers: answers);

    Scaffold homepage = Scaffold(
        appBar: AppBar(title: Text('Homepage')),
        body: (showAnswers) ? bodyAnswers : bodyQuestions
    );
    return MaterialApp(
        home: homepage
    );
    }
}
// The invisible widgets are Container and Scaffold.
// The visible widgets are AppBar and Text.
// To specify margin or padding spacing, we can use EdgeInsets.
// The values for spacing are in multiples of 8(e.g 16, 24, 32).
// To add spacing on all sides, use EdgeInsets.all().
//To add spacing on certain sides, use EdgeInsets.only(direction: value).
// In Flutter, width = double.infinity is the equivalent of width = 100% in CSS.
// To put colors on a container, the decoration: BoxDecoration(color: Colors.green) can be used
// In a Column widget, the main axis alignment is vertical (or from top to bottom).
// To change the horizontal alignment of widgets in a column, we must use crossAxisAlignment.
// For example, if we want to place column widgets to the horizontal left, we set CrossAxisAlignment.start.
// In the context of the column, we can consider the column as vertical , and its cross axis as horizontal.
// The scaffold widget provides basic desing layout structure.
// The scaffold can be given UI elements such as an app bar.