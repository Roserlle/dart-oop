//The Keyword 'void' means the function will not return anything
//The syntax of a function is: return-type function-name(parameters)
//Dart does not allow null value
void main() {
  //console.log
  String firstName = 'John';
  //If we have null value, to accept it, we can add question mark (?) in data type
  String? middleName = null;
  String lastName = 'Smith';
  int age = 31; //for numbers with decimal points
  double height = 172.45; //for numbers with decimal points
  num weight = 64.32; //can accept numbers with or without decimal points
  bool isRegistered = false; //Boolean
  List<num> grades = [98.2, 89, 87.88, 91.2]; //List of an array
  Map<String, dynamic> personA = {
    //Map is an object
    'name': 'Brandon',
    'batch': 213
  };
  //Alternative syntax for declaring object

  //dynamic - we can use it habang nagpprocess
  Map<String, dynamic> personB = new Map();
  personB['name'] = 'Juan';
  personB['batch'] = 89;

  //With final, once value has been set, it cannot be changed
  final DateTime now;
  now = DateTime.now();

  //dapat meron na agad sya value once na create, analogy is like the value of Pi, cannot be declare later
  //With const, an identifier must have a corresponding declaration of value
  // const String companyAcronym = 'FFUF';
  // companyAcronym = 'FFUF';

  // print(firstName + ' ' + lastName); //concatenation
  print('Full Name: $firstName $middleName $lastName');
  print('Age: ' + age.toString());
  print('Height: ' + height.toString());
  print('Weight: ' + weight.toString());
  print('isRegistered: ' + isRegistered.toString());
  print('Grades: ' + grades[0].toString());
  print('Current DateTime: ' + now.toString());

  //To access a value from a Map
  print(personA['name']);
  for (var data in grades) {
    print(data);
  }
  personA.forEach((key, value) {
    print(key);
  });
}
