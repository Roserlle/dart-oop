void main() {
  Equipment equipment = new Equipment();
  equipment.name = 'Equipment-001';
  print(equipment.name);

  Loader loader = new Loader();
  loader.name = 'Loader-001';
  print(loader.name);
  print(loader.getCategory());

  Crane wheelLoader = new Crane();
  wheelLoader.name = 'Crane-001';
  print(wheelLoader.name);
  print(wheelLoader.getCategory());
}

class Equipment {
  String? name;
}

class Loader extends Equipment {
  String getCategory() {
    return '${this.name} is a loader';
  }
}

class Crane extends Loader {
  String getCategory() {
    return '${this.name} is a wheel crane.';
  }
}
