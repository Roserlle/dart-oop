void main() {
  Person person = new Person(lastName: 'Smith', firstName: 'John');
  Employee employee = new Employee(firstName: 'Westwood', lastName: 'Jonas');

  print(person.getFullName());
  print(employee.getFullName());
}

class Person {
  String firstName;
  String lastName;

  Person({required this.firstName, required this.lastName});

  String getFullName() {
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person {
  Employee({required String firstName, required String lastName})
      : super(firstName: 'firstName', lastName: 'lastName');
}
